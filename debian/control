Source: acme-tiny
Maintainer: Debian Let's Encrypt Team <team+letsencrypt@tracker.debian.org>
Uploaders: Jeremías Casteglione <jrmsdev@gmail.com>
Section: utils
Priority: optional
Build-Depends: debhelper (>= 11~),
               dh-python,
               python3 (>= 3.3.2-2~),
               python3-setuptools
Standards-Version: 4.1.4
Homepage: https://github.com/diafygi/acme-tiny
Vcs-Git: https://salsa.debian.org/letsencrypt-team/acme-tiny.git
Vcs-Browser: https://salsa.debian.org/letsencrypt-team/acme-tiny

Package: acme-tiny
Architecture: all
Depends: openssl (>=1.0.1k),
         python3-pkg-resources,
         ${misc:Depends},
         ${python3:Depends}
Description: letsencrypt tiny Python client
 acme-tiny is a tiny script to issue and renew TLS certs from Let's Encrypt
 .
 This is a tiny, auditable script that you can throw on your server to issue and
 renew Let's Encrypt certificates. Since it has to be run on your server and
 have access to your private Let's Encrypt account key, the script is kept as
 tiny as possible (currently less than 200 lines). The only prerequisites are
 Python and openssl.
